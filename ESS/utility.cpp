//
// Created by Myrthan on 11/11/17.
//

#include "utility.h"

int utility::toInd(int x, int y, int n) {
    return x + y*n;
}

std::tuple<int,int> utility::toCoord(int i, int n) {
    return std::make_tuple(i % n, i / n);
}

void utility::debugPrint(std::string msg, Region f_split) {
    std::cout << msg << f_split.get_l().first << ", " << f_split.get_l().second << ")";
    std::cout << "r:(" << f_split.get_r().first << ", " << f_split.get_r().second << ")";
    std::cout << "t:(" << f_split.get_t().first << ", " << f_split.get_t().second << ")";
    std::cout << "b:(" << f_split.get_b().first << ", " << f_split.get_b().second << ")";
}

void utility::debugResultPrint(Region max_region) {
    std::cout << "Found box:" << std::endl;
    std::cout << "l:" << max_region.get_l().first <<" " <<max_region.get_l().second;
    std::cout << "r:" << max_region.get_r().first << " " <<max_region.get_r().second;
    std::cout << "t:" <<max_region.get_t().first <<" " <<max_region.get_t().second;
    std::cout << "b:" << max_region.get_b().first << " " << max_region.get_b().second;
};
/*
 * Debug cases:
 * //    n = 5; m = 5;
//    img_feature_points.clear();
//    img_feature_points.push_back({1., 1., 0});
//    img_feature_points.push_back({1., 2., 0});
//    img_feature_points.push_back({2., 3., 2});
//    img_feature_points.push_back({0., 2., 2});
//    img_feature_points.push_back({2., 1., 1});
//    img_feature_points.push_back({4., 4., 2});
//    double t[3];
//    t[0] = 100; t[1] = -50, t[2] = -100;
//    cluster_weights = t;
 */
