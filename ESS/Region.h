//
// Created by Myrthan on 11/11/17.
//

#ifndef OPENCV_Region_H
#define OPENCV_Region_H

#include "iostream"

class Region {
    // intervals order t, b, l, r
public:
    int *interval, size = 0;
    Region() = default;
    Region(int *_interval, int _size);
    Region(const Region &r);
    Region(Region&&r);
    Region& operator=(const Region &other);
    Region& operator=(Region &&other);
    bool operator<(Region const& rhs) const;
    bool correctRectangle() const;
    ~Region();
    int & operator[](std::size_t i);
    /*
     * Warning, all methods below assume that interval is not null.
     */
    // quality function
    int operator()() const;
    int getMaxIndex();
    // check if region has only one Region
    bool hasSingleRectangle();
    std::pair<int,int> get_t() const;
    std::pair<int,int> get_b() const;
    std::pair<int,int> get_l() const;
    std::pair<int,int> get_r() const;
    int get_t_len() const;
    int get_b_len() const;
    int get_l_len() const;
    int get_r_len() const;
};


#endif //OPENCV_Region_H
