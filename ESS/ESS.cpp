//
// Created by Myrthan on 11/10/17.
//

#include "ESS.h"
#include "math.h"

typedef Region R;
typedef long double (*quality_bound_function)(R, double*, double*, int, int);

std::tuple<R, R> splitRegion(R, int);
bool singleRegion(Region r);


/*
 * efficient subwindow search with data preprocessing
 *
*/
Region findMaxRegion(std::vector<std::tuple<double, double, int>> img_feature_points, int n, int m, double * cluster_weights, int num_clusters) {
    double positive_weights_matrix[n*m];
    double negative_weights_matrix[n*m];
    for (int i = 0; i < n*m; i++) positive_weights_matrix[i] = negative_weights_matrix[i] = 0;
    for (auto t: img_feature_points) {
        int x = std::get<0>(t);
        int y = std::get<1>(t);
        int cluster_ind = std::get<2>(t);
        double cluster_weight = cluster_weights[cluster_ind];
        if (cluster_weight > 0.) positive_weights_matrix[utility::toInd(x,y,n)] += cluster_weight;
        else negative_weights_matrix[utility::toInd(x,y,n)] += cluster_weight;
    }

    quality_bound::precomputeMatrixSum(positive_weights_matrix, n, m);
    quality_bound::precomputeMatrixSum(negative_weights_matrix, n, m);

    /*
     * Branch and bound serach
     * currMaxRegion is the most optimal candidate region from top of the queue
     *
     */
    quality_bound_function f = quality_bound::f_;
    std::priority_queue<std::pair<long double, R>> P;
    // intervals t:[0,n-1] b:[0,n-1] l:[0,m-1] r:[0,m-1], size (fixed)
    R currMaxRegion(new int[8]{0, n-1, 0, n-1, 0, m-1, 0, m-1}, 8);
    do {
        // find index of splitting
        int splittingIndex =  currMaxRegion.getMaxIndex();
        // if legal splitting cannot be found continue
        if (splittingIndex >= 0) {
            // split region along the splittingIndex interval [t, b, l, r]
            auto[f_split, s_split] = std::move(splitRegion(currMaxRegion, splittingIndex));
            if (f_split.correctRectangle()) {
                long double val = f(f_split, positive_weights_matrix, negative_weights_matrix, n, m);
                P.push({val, f_split});
            }
            if (s_split.correctRectangle()) {
                long double val = f(s_split, positive_weights_matrix, negative_weights_matrix, n, m);
                P.push({val, s_split});
            }
         }
        currMaxRegion = P.top().second;
        P.pop();
    } while(!currMaxRegion.hasSingleRectangle());

    return currMaxRegion;
}

/*
 * Splitting code
 *
 */
std::tuple<R, R> splitRegion(R r_l, int splittingIndex) {
    R r_r(r_l);
    int old_interval_l = r_l[splittingIndex];
    int old_interval_r = r_l[splittingIndex+1];
    r_l[splittingIndex+1] = (old_interval_l+old_interval_r)/2;
    r_r[splittingIndex] = (old_interval_l+old_interval_r+1)/2;
    return std::make_tuple(r_l, r_r);
}

/*
 * Multiple search methods
 */

void deleteObject(int **I, R obj) {
}