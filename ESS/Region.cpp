//
// Created by Myrthan on 11/11/17.
//

#include "Region.h"


Region::Region(int *_interval, int _size): interval(_interval), size(_size)  {}
Region::~Region() {
    if (interval) delete interval;
}
Region::Region(const Region &r) {
    interval = new int[r.size];
    for (int i = 0; i < r.size; i++) interval[i] = r.interval[i];
    size = r.size;
}
Region::Region(Region &&r): interval(r.interval), size(r.size) {
    r.interval = nullptr; r.size = 0;
}
Region& Region::operator=(const Region &r) {
    if (this != &r)  {
        interval = new int[r.size];
        for (int i = 0; i < r.size; i++) interval[i] = r.interval[i];
        size = r.size;
    }
    return *this;
}
Region& Region::operator=(Region &&r) {
    if (this != &r)  {
        interval = r.interval;
        size = r.size;
        r.interval = nullptr;
        r.size = 0;
    }
    return *this;
}
bool Region::operator<(Region const& r) const {
    return true;
}
bool Region::correctRectangle() const{
    return get_l().first < get_r().second && get_t().first < get_b().second;
}
int Region::getMaxIndex() {
    int best_ind = -1, max_interval = 0;
    for (int i = 0; i < size; i+=2) {
        int curr_interval = interval[i+1] - interval[i];
        if (curr_interval > max_interval) {
            best_ind = i;
            max_interval = curr_interval;
        }
    }
    return best_ind;
}
int Region::get_t_len() const {
    return interval[1]-interval[0];
}
int Region::get_b_len() const {
    return interval[3]-interval[2];
}
int Region::get_l_len() const {
    return interval[5]-interval[4];
}
int Region::get_r_len() const {
    return interval[7]-interval[6];
}
std::pair<int,int> Region::get_t() const {
    return {interval[0], interval[1]};
}
std::pair<int,int> Region::get_b() const {
    return {interval[2], interval[3]};
}
std::pair<int,int> Region::get_l() const {
    return {interval[4],interval[5]};
}
std::pair<int,int> Region::get_r() const {
    return {interval[6], interval[7]};
}
bool Region::hasSingleRectangle() {
    return get_t_len() == 0 && get_b_len() == 0 &&  get_l_len() == 0 && get_r_len() == 0;
}
int & Region::operator[](size_t i) {
    return interval[i];
}
/*
 * quality function
 */
int Region::operator()() const {
    return -1;
}