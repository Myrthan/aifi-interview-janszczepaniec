//
// Created by Myrthan on 11/11/17.
//

#ifndef OPENCV_QUALITY_BOUND_H
#define OPENCV_QUALITY_BOUND_H

#include "Region.h"
#include "iostream"

namespace quality_bound {
    /*
     * Code for computing sum
     * s.t. every cell with coordiantes (x,y)
     * keep sum of rectangle (0,0) -> (x,y)
     */
    void precomputeMatrixSum(double *M, int n, int m);

    /*
     * Bound function ....
     */
    long double f_(Region, double * positive, double * negative, int n, int m);
}


#endif //OPENCV_QUALITY_BOUND_H
