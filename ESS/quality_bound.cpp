//
// Created by Myrthan on 11/11/17.
//

#include "quality_bound.h"
#include "utility.h"

void quality_bound::precomputeMatrixSum(double *M, int n, int m) {
    for (int x = 1; x < n; x++) M[utility::toInd(x,0,n)] += M[utility::toInd(x-1,0,n)];
    for (int y = 1; y < m; y++) {
        M[utility::toInd(0,y,n)] += M[utility::toInd(0,y-1,n)];
        for (int x = 1; x < n; x++) {
            M[utility::toInd(x,y,n)] += M[utility::toInd(x,y-1,n)] + M[utility::toInd(x-1,y,n)] - M[utility::toInd(x-1,y-1,n)];
        }
    }
}

/*
 * (x1, y1) left up corner
 * (x2, y2) right down corner
 */
long double compute_summed_area(double * in, int n, int x1, int y1, int x2, int y2) {
    if (x1 > x2 || y1 > y2) return 0;
    long double sum = in[utility::toInd(x2, y2, n)];
    if (x1 > 0 && y1 > 0) sum += in[utility::toInd(x1-1, y1-1, n)];
    if (x1 > 0) sum -= in[utility::toInd(x1-1, y2, n)];
    if (y1 > 0) sum -= in[utility::toInd(x2, y1-1, n)];
    return  sum;
}

long double quality_bound::f_(Region r, double * positive, double * negative, int n, int m) {
    // R_max:
    long double f_plus = compute_summed_area(positive, n, r.get_l().first, r.get_t().first, r.get_r().second, r.get_b().second);
    // R_min
    long double f_minus = compute_summed_area(negative, n, r.get_l().second, r.get_t().second, r.get_r().first, r.get_b().first);
    return f_plus+f_minus;
}
