//
// Created by Myrthan on 11/11/17.
//

#ifndef OPENCV_UTILITY_H
#define OPENCV_UTILITY_H

#include "iostream"
#include "Region.h"

namespace utility {
    int toInd(int x, int y, int n);
    std::tuple<int,int> toCoord(int i, int n);
    void debugPrint(std::string msg, Region f_split);
    void debugResultPrint(Region max_region);
}

#endif //OPENCV_UTILITY_H
