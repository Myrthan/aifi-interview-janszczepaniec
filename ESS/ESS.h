//
// Created by Myrthan on 11/10/17.
//

#ifndef OPENCV_ESS_H
#define OPENCV_ESS_H

#include <stdio.h>
#include <initializer_list>
#include "iostream"
#include <queue>
#include "Region.h"
#include "utility.h"
#include "vector"
#include "quality_bound.h"

Region findMaxRegion(std::vector<std::tuple<double, double, int>> img_feature_points, int n, int m, double * cluster_weights, int num_clusters);

#endif //OPENCV_ESS_H
