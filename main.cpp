//
// Created by Myrthan on 11/10/17.
//
// code comes from opencv tutorial
// https://docs.opencv.org/3.0-beta/doc/tutorials/features2d/feature_description/feature_description.html#feature-description

#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "ESS/ESS.h"
#include "ESS/Region.h"
#include <math.h>

using namespace cv;
using namespace cv::xfeatures2d;

/*
 * Input data:
 * Image width, height: n:int, m:int
 * Number of img feature points: f:int
 * f features points in form (x,y,class), every in new line: int int int
 * Number of classes: c
 * c weights from svm vector, every in new line: double
 */

/** @function main */
int main( int argc, char** argv )
{
    int width, height, c;
    double x, y;
    int features_point_size, cluster_weight_size;
    std::cin >> width >> height;
    std::cin >> features_point_size;
    std::vector<std::tuple<double, double,int>> features;
    for (int i = 0; i < features_point_size; i++) {
        std::cin >> x >> y >> c;
        features.push_back({x,y,c});
    }
    std::cin >> cluster_weight_size;
    double cluster_weights[cluster_weight_size];
    for (int i = 0; i < cluster_weight_size; i++) {
        std::cin >> cluster_weights[i];
    }

    Region max_region = findMaxRegion(features, width, height, cluster_weights, cluster_weight_size);

//    utility::debug_result(max_region);
    std::cout << max_region.get_l().first << " " << max_region.get_t().first << " " << max_region.get_r().second << " " << max_region.get_b().second << std::endl;
    return 0;
}
