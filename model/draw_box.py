import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np
import sys
import cv2

filename = sys.argv[1]
x1 = int(sys.argv[2])
y1 = int(sys.argv[3])
x2 = int(sys.argv[4])
y2 = int(sys.argv[5])
img = np.array(Image.open(filename), dtype=np.uint8)
cv2.rectangle(img,(x1, y1),(x2,y2),(0,255,0),5)
# Create figure and axes
fig,ax = plt.subplots(1)

# Display the image
ax.imshow(img)

plt.show()