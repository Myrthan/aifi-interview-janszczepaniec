from sklearn.externals import joblib
import cv2
from sklearn import svm
from scipy.cluster.vq import vq
import numpy as np
import sys
weights_file_name = sys.argv[1]
codebook_name = sys.argv[2]
img_name = sys.argv[3]

clf = joblib.load(weights_file_name)
detector = cv2.FeatureDetector_create("SIFT")
extractor = cv2.DescriptorExtractor_create("SIFT")

img = cv2.imread(img_name)
kp = detector.detect(img)
kp, desp = extractor.compute(img, kp)

codebook = np.load(codebook_name)
codes, _ = vq(desp, codebook)

width, height, channels = img.shape
print height, width
print len(codes)
for i in range(len(codes)):
	print kp[i].pt[0], kp[i].pt[1], codes[i]

print len(clf.coef_[0])
for i in clf.coef_[0]:
	print i
