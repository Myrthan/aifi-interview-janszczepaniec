import cv2
import numpy as np
import os
from scipy.cluster.vq import vq, kmeans, whiten
import matplotlib.pyplot as plt
from sets import Set
import xml.etree.ElementTree as ET
import pickle
import re
from sklearn.svm import LinearSVC
from sklearn.externals import joblib
from tempfile import TemporaryFile
import sys
def get_paths(path, file_names):
	return [os.path.join(path, name)for name in file_names]

weights_name_file = sys.argv[1]
codebook_name_file = sys.argv[2]


# Fixed paths to dataset
fixed_train_imgs_path = "./VOCdevkit/VOC2007/JPEGImages/"
fixed_train_annotations_path = "./VOCdevkit/VOC2007/Annotations/"
fixed_train_labels_path = "./VOCdevkit/VOC2007/ImageSets/Main/"

# Load dataset names
train_annotation_names = os.listdir(fixed_train_annotations_path)
cat_labels = np.loadtxt(os.path.join(fixed_train_labels_path, "cat_train.txt"), dtype={'names': ('name', 'label'), 'formats': ('S6', 'i4',)})
dog_labels = np.loadtxt(os.path.join(fixed_train_labels_path, "dog_train.txt"), dtype={'names': ('name', 'label'), 'formats': ('S6', 'i4',)})
cat_dog_labels = np.append(np.extract([x[1] == 1 for x in cat_labels], cat_labels), np.extract([x[1] == 1 for x in dog_labels], dog_labels))
# train_label_paths = os.imlist(fixed_train_labels_path)

# SIFT feature extraction
detector = cv2.FeatureDetector_create("SIFT")
extractor = cv2.DescriptorExtractor_create("SIFT")
processed_img_names = set(cat_dog_labels[:][0])

descriptors_list = []
limit = 100
w = 0
for (name, label) in cat_dog_labels:
	path = os.path.join(fixed_train_imgs_path, name + ".jpg")
	img = cv2.imread(path)
	kps = detector.detect(img)
	kps, desps = extractor.compute(img, kps)
	annot_path = os.path.join(fixed_train_annotations_path, name  + ".xml")
	root = ET.parse(annot_path).getroot()
	boxes = []
	class_id = 0
	for box in root.iterfind('object'):
		class_name = box.find('name')
		if class_name.text == "cat" or class_name.text == "dog":			
			xmin = int(box.find("bndbox/xmin").text)
			ymin = int(box.find("bndbox/ymin").text)
			xmax = int(box.find("bndbox/xmax").text)
			ymax = int(box.find("bndbox/ymax").text)
			boxes.append((class_id, xmin, ymin, xmax, ymax))
			class_id+=1
	kp_negatives = []
	desp_negatives = []
	kp_positives =  [[] for i in range(len(boxes))]
	desp_positives = [[] for i in range(len(boxes))]
	for i in xrange(len(kps)):
		kp = kps[i]
		desp = desps[i]
		x = kp.pt[0]
		y = kp.pt[1]
		for (class_id, xmin, ymin, xmax, ymax) in boxes:
			if xmin <= x and ymin <= y and x <= xmax and y <= ymax:
				kp_positives[class_id].append(kp)
				desp_positives[class_id].append(desp)
			else:
				kp_negatives.append(kp)
				desp_negatives.append(desp)
	if len(kp_negatives) > 0:
		descriptors_list.append((path, kp_negatives, desp_negatives, 1))
	if len(kp_positives) > 0:
		for box_id in range(len(kp_positives)):
			descriptors_list.append((path, kp_positives[box_id], desp_positives[box_id], 0))

processed_img_names = set(np.array([x for (x, _) in cat_dog_labels]))

train_img_names = os.listdir(fixed_train_imgs_path)
train_img_paths = get_paths(fixed_train_imgs_path, train_img_names)
train_img_names_paths = np.column_stack((train_img_names, train_img_paths))

# developer code for testing
limit = 5000 - len(cat_dog_labels)
i = 0
for (name, path) in train_img_names_paths:
	if re.sub('\.jpg$', '', name) not in processed_img_names:
		img = cv2.imread(path)
		kp = detector.detect(img)
		kp, desp = extractor.compute(img, kp)
		descriptors_list.append((path, kp, desp, 0))
		if i == limit:
			break
		i+=1

img_paths = descriptors_list[:][0]
kps = descriptors_list[0][1]
descriptors = descriptors_list[0][2]
id_classes = np.array([x for (_, _, _, x) in descriptors_list])

for _, kp, desp, _ in descriptors_list[1:]:
	descriptors = np.vstack(desp)
	kps = np.vstack(kp)

# retrieve labels from features if point is in truth bounding box got label 0 otherwise 1
# perform kmeans clustering then calcualte histogram
# https://docs.scipy.org/doc/scipy-0.7.x/reference/cluster.vq.html
k = 1000
codebook, distortion = kmeans(descriptors, k, 1) 
histogram = np.zeros((len(descriptors_list), k), "float32")
for i in xrange(len(descriptors_list)):
    codes, _ = vq(descriptors_list[i][2], codebook)
    for code in codes:
        histogram[i][code] += 1

# train svm
clf = LinearSVC()
clf.fit(histogram, np.array(id_classes))

# save svm weights for ESS
joblib.dump(clf, weights_name_file + ".pkl") 
np.save(codebook_name_file, codebook) 
# np.save("descriptors_1_negatives",codebook) 
