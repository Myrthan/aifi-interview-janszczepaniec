#!/bin/bash
wget -r http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
wget -r http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar

tar xvzf ./host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
tar xvzf ./host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar

rm -rf host.robots.ox.ac.uk

mkdir dataset
mkdir ./dataset/test
mkdir ./dataset/train
mv * ./VOCdevkit/VOC2007/* ./dataset/train/
mv * ./VOCdevkit\ 2/VOC2007/* ./dataset/test/

