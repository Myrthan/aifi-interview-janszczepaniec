Repository for Efficient Subwindow Search implementation&evaluation
Based on https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/34843.pdf

Author: Jan Szczepaniec

This is part of interview process at Aifi (http://aifi.io).

Code usage:*
Data set preparation: ./load_data.sh

Train SVM: ./model.py output_weight_file_name output_codebook_file_name

Input preparation: python runsvm.py svm_weight_file_name code__book_name img_name > input

Run ESS: main.cpp: ./cmake-build-debug/opencv < input > result_box

where result_box is 4 int x1, y1, x2, y2 

where (x1,y1) is top left corner and

where (x2,y2) is low right corner

Draw Box: python draw_box img x1 y1 x2 y2


Note* all python files are in folder model